# zsh-prompt-with-git

Provides a ZSH prompt with git repo information + a colored prompt

## Installation

- copy the `zsh` directory to `~/.zsh` (or a name of your choice, `~/.titi` for instance)
- add to the file `~/.zshrc`: `source ~/.zsh/config-prompt-zsh.sh` (or `source ~/.titi/config-prompt-zsh.sh` if you choose `~/.titi` in the first place)
- you are done, open a new terminal and check the result in a normal directory and in a git repository

## How does it work? how can I update it?

- the `zsh/git-prompt.sh` was taken from the [official git repository](https://github.com/git/git/blob/master/contrib/completion/git-prompt.sh)
- the `zsh/_git` file was taken from the [official git repository](https://github.com/git/git/blob/master/contrib/completion/git-completion.zsh)
- the `zsh/git-completion.bash` file was taken from the [official git repository](https://github.com/git/git/blob/master/contrib/completion/git-completion.bash)

So you can update these files thanks to the official github pages.

Each file provides useful information on how to use/install it, in the comments at the beginning of the file.

## How to configure git prompt?

You can configure the git prompt with supported options (described in the `zsh/git-prompt.sh` file).

To change it, open the `~/.zsh/config-prompt-zsh.sh` file and for instance change this line: 
```
export GIT_PS1_SHOWCOLORHINTS=true
```

## How to configure the prompt colors and misc infos?

Open file `~/.zsh/config-prompt-zsh.sh`.

You can tune the following line to update colors and info (RTFM ;) ):

```
setopt PROMPT_SUBST ; PS1='%B%F{cyan}%~%f%b$(__git_ps1 " (%s)") %(?.%F{green}√.%F{red}?%?)%f '
```

By default it provides:
 - relative path in cyan
 - git status if pertinent
 - a green or red mark depending on the last command status
