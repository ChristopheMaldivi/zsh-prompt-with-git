source ~/.zsh/git-prompt.sh

# Load Git completion
# source: https://oliverspryn.medium.com/adding-git-completion-to-zsh-60f3b0e7ffbc
zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.bash
fpath=(~/.zsh $fpath)
autoload -Uz compinit && compinit

export GIT_PS1_SHOWCOLORHINTS=true
export GIT_PS1_SHOWUNTRACKEDFILES=true
export GIT_PS1_SHOWDIRTYSTATE=true

# Prompt with last command status + git integration
setopt PROMPT_SUBST ; PS1='%B%F{cyan}%~%f%b$(__git_ps1 " (%s)") %(?.%F{green}√.%F{red}?%?)%f '

# Display date on the right
export RPROMPT='%*'

